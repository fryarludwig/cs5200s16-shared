// <copyright file="PlayerTest.cs">Copyright ©  2016</copyright>
using System;
using DSoakPlayer;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SharedObjects;

namespace DSoakPlayer.Tests
{
    /// <summary>This class contains parameterized unit tests for Player</summary>
    [PexClass(typeof(Player))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class PlayerTest
    {
        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        internal Player ConstructorTest()
        {
            Player target = new Player();
            return target;
        }

        /// <summary>Test stub for Error(String)</summary>
        [PexMethod]
        internal void StartAndStop([PexAssumeUnderTest]Player target)
        {
            target.Start();
            System.Threading.Thread.Sleep(500);
            Assert.IsTrue(target.IsActive());
            target.Stop();

            System.Threading.Thread.Sleep(50);
            Assert.IsFalse(target.IsActive());

            target.Stop();
            Assert.IsFalse(target.IsActive());

            target.Start();
            System.Threading.Thread.Sleep(50);
            Assert.IsTrue(target.IsActive());
            target.Stop();
            System.Threading.Thread.Sleep(50);
            Assert.IsFalse(target.IsActive());
        }
    }
}
