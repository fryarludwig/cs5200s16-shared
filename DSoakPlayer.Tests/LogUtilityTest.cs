using System;
using DSoakPlayer;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSoakPlayer.Tests
{
    /// <summary>This class contains parameterized unit tests for LogUtility</summary>
    [TestClass]
    [PexClass(typeof(LogUtility))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class LogUtilityTest
    {

        /// <summary>Test stub for .ctor()</summary>
        [PexMethod]
        internal LogUtility ConstructorTest()
        {
            LogUtility target = new LogUtility("TestLogger");
            return target;
        }

        /// <summary>Test stub for Info(String)</summary>
        [PexMethod]
        internal void InfoTest([PexAssumeUnderTest]LogUtility target, string logMessage)
        {
            target.Info(logMessage);
        }

        /// <summary>Test stub for Warn(String)</summary>
        [PexMethod]
        internal void WarnTest([PexAssumeUnderTest]LogUtility target, string logMessage)
        {
            target.Warn(logMessage);
        }

        /// <summary>Test stub for Error(String)</summary>
        [PexMethod]
        internal void ErrorTest([PexAssumeUnderTest]LogUtility target, string logMessage)
        {
            target.Error(logMessage);
        }

        /// <summary>Test stub for Trace(String)</summary>
        [PexMethod]
        internal void TraceTest([PexAssumeUnderTest]LogUtility target, string logMessage)
        {
            target.Trace(logMessage);
        }

        /// <summary>Test stub for .ctor(String)</summary>
        [PexMethod]
        internal LogUtility ConstructorTest(string loggerName)
        {
            LogUtility target = new LogUtility(loggerName);
            return target;
        }
    }
}
