using System;
using DSoakPlayer;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSoakPlayer.Tests
{
    /// <summary>This class contains parameterized unit tests for LogHelper</summary>
    [TestClass]
    [PexClass(typeof(LogHelper))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    public partial class LogHelperTest
    {

        /// <summary>Test stub for Log(Level, String, String)</summary>
        [PexMethod]
        public void LogTest(
            Level logLevel,
            string source,
            string message
        )
        {
            LogHelper helper = new LogHelper();
            int counter = 5;
            if (!helper.IsActive() && counter-- > 0)
            {
                System.Threading.Thread.Sleep(100);
            }

            PexAssume.IsTrue(helper.IsActive());
            helper.Log(logLevel, source, message);
            helper.Log(Level.TRACE, "LogUtilityTest", "message1");
            helper.Log(Level.INFO, "LogUtilityTest", "message2");
            helper.Log(Level.WARN, "LogUtilityTest", "message3");
            helper.Log(Level.ERROR, "LogUtilityTest", "message4");

            System.Threading.Thread.Sleep(100);
        }
    }
}
