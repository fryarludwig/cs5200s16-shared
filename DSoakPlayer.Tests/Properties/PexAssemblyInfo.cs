using System;
using System.Globalization;
using Microsoft.Pex.Framework.Suppression;
// <copyright file="PexAssemblyInfo.cs">Copyright ©  2016</copyright>
using Microsoft.Pex.Framework.Coverage;
using Microsoft.Pex.Framework.Creatable;
using Microsoft.Pex.Framework.Instrumentation;
using Microsoft.Pex.Framework.Settings;
using Microsoft.Pex.Framework.Validation;

// Microsoft.Pex.Framework.Settings
[assembly: PexAssemblySettings(TestFramework = "VisualStudioUnitTest")]

// Microsoft.Pex.Framework.Instrumentation
[assembly: PexAssemblyUnderTest("DSoakPlayer")]
[assembly: PexInstrumentAssembly("System.Windows.Forms")]

// Microsoft.Pex.Framework.Creatable
[assembly: PexCreatableFactoryForDelegates]

// Microsoft.Pex.Framework.Validation
[assembly: PexAllowedContractRequiresFailureAtTypeUnderTestSurface]
[assembly: PexAllowedXmlDocumentedException]

// Microsoft.Pex.Framework.Coverage
[assembly: PexCoverageFilterAssembly(PexCoverageDomain.UserOrTestCode, "System.Windows.Forms")]
[assembly: PexInstrumentType("mscorlib", "System.Runtime.CompilerServices.JitHelpers")]
[assembly: PexSuppressUninstrumentedMethodFromType("System.DateTimeFormat")]
[assembly: PexSuppressUninstrumentedMethodFromType(typeof(DateTimeFormatInfo))]
[assembly: PexSuppressUninstrumentedMethodFromType(typeof(TimeZoneInfo))]
[assembly: PexSuppressUninstrumentedMethodFromType(typeof(Enum))]

