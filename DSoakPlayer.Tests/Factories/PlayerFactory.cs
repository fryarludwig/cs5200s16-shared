using DSoakPlayer;
using System;
using Microsoft.Pex.Framework;

using SharedObjects;

namespace DSoakPlayer
{
    /// <summary>A factory for DSoakPlayer.Player instances</summary>
    public static partial class PlayerFactory
    {
        /// <summary>A factory for DSoakPlayer.Player instances</summary>
        [PexFactoryMethod(typeof(DSoakPlayerWindow), "DSoakPlayer.Player")]
        public static Player Create(
            string value_s,
            string value_s1,
            string value_s2,
            string value_s3
        )
        {
            Player player = new Player();
            player.Identity.FirstName = value_s;
            player.Identity.LastName = value_s1;
            player.Identity.ANumber = value_s2;
            player.Identity.Alias = value_s3;
            return player;
        }
    }
}
