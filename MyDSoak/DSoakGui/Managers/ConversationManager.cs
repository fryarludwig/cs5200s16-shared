﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using DSoakGui.Common;
using DSoakGui.Utilities;

namespace DSoakGui.Managers
{
    class ConversationManager : Threaded
    {
        public ConversationManager()
        {
            NetworkManager = new NetworkManager();
            SendMessageQueue = new ConcurrentQueue<Envelope>();
            conversationDictionary = new Dictionary<SharedObjects.MessageNumber, Conversation>();
        }

        public Conversation CreateConversation(Envelope envelope)
        {
            Conversation newConversation = new Conversation();
            newConversation.Id = ConversationCounter++;

            return newConversation;
        }

        protected override void run()
        {
            while (!threadDone)
            {
                // Check if there are any messages to be received
                if (!NetworkManager.InboundQueue.IsEmpty)
                {
                    Envelope result;
                    if (NetworkManager.InboundQueue.TryDequeue(out result))
                    {

                        if (!conversationDictionary.ContainsKey(result.Message.ConvId))
                        {
                            Conversation tempConv = CreateConversation(result);
                            conversationDictionary.Add(result.Message.ConvId, tempConv);
                        }

                        conversationDictionary[result.Message.ConvId].MessageQueue.Enqueue(result);

                    }
                }
                // Check if there are any messages to be sent
                else if (!SendMessageQueue.IsEmpty)
                {
                    Envelope result;
                    if (SendMessageQueue.TryDequeue(out result))
                    {
                        NetworkManager.OutboundQueue.Enqueue(result);
                    }
                }
                // There is nothing to be done right now
                else
                {
                    Thread.Sleep(100);
                }
            }
        }

        protected override void derivedStop()
        {

        }

        private ConcurrentQueue<Envelope> SendMessageQueue;

        private NetworkManager NetworkManager;
        private static UInt32 ConversationCounter = 0;
        private static Dictionary<SharedObjects.MessageNumber, Conversation> conversationDictionary;
    }
}
