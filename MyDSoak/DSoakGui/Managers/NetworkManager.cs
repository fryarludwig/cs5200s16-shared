﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Messages;

using DSoakGui.Common;
using DSoakGui.Utilities;

namespace DSoakGui.Managers
{
    class NetworkManager : Threaded
    {
        public NetworkManager()
        {
            LocalPort = 0;
            InboundQueue = new ConcurrentQueue<Envelope>();
            OutboundQueue = new ConcurrentQueue<Envelope>();
        }

        protected override void run()
        {
            UdpClient socket = new UdpClient(LocalPort);
            socket.Client.ReceiveTimeout = 2000;

            try
            {
                while (!threadDone)
                {
                    if (socket.Available > 0)
                    {
                        IPEndPoint endpoint = new IPEndPoint(IPAddress.Any, LocalPort);
                        byte[] bytesReceived = socket.Receive(ref endpoint);
                        if (bytesReceived.Length > 0)
                        {
                            Message recvdMessage = Message.Decode(bytesReceived);
                            Envelope tempEnvelope = new Envelope(endpoint, recvdMessage);
                            if (recvdMessage != null)
                            {
                                InboundQueue.Enqueue(tempEnvelope);
                            }
                        }
                    }
                    else if (!OutboundQueue.IsEmpty)
                    {
                        Envelope outboundEnvelope;
                        if (OutboundQueue.TryDequeue(out outboundEnvelope))
                        {
                            byte[] bytesToSend = outboundEnvelope.Message.Encode();
                            if (bytesToSend.Length > 0)
                            {
                                socket.Send(bytesToSend, bytesToSend.Length, outboundEnvelope.Address);
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                }
            }
            catch (Exception exc)
            {
                Logger.log.Error("UDP socket exception : " + exc.Message);
            }

            socket.Close();
            Logger.log.Error("Closing down Network Manager");
        }

        protected void updateGui()
        {
            //DSoakGui.Program.GuiWindow.Invoke((System.Windows.Forms.MethodInvoker)(delegate
            //{
            //    DSoakGui.Program.GuiWindow.updateConnectionStatus("Connected to server");
            //}));
        }

        public int LocalPort;
        public ConcurrentQueue<Envelope> InboundQueue;
        public ConcurrentQueue<Envelope> OutboundQueue;
    }
}
