﻿namespace DSoakGui
{
    partial class mainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.loginButton = new System.Windows.Forms.Button();
            this.firstNameTextbox = new System.Windows.Forms.TextBox();
            this.gameListCombo = new System.Windows.Forms.ComboBox();
            this.lastNameTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.aNumberTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.aliasTextbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ipTextbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.portTextbox = new System.Windows.Forms.TextBox();
            this.joinButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "First Name";
            // 
            // loginButton
            // 
            this.loginButton.Location = new System.Drawing.Point(72, 152);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(75, 23);
            this.loginButton.TabIndex = 2;
            this.loginButton.Text = "Log In";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.onLoginButtonClicked);
            // 
            // firstNameTextbox
            // 
            this.firstNameTextbox.Location = new System.Drawing.Point(72, 12);
            this.firstNameTextbox.Name = "firstNameTextbox";
            this.firstNameTextbox.Size = new System.Drawing.Size(119, 20);
            this.firstNameTextbox.TabIndex = 3;
            this.firstNameTextbox.Text = "Susan";
            // 
            // gameListCombo
            // 
            this.gameListCombo.FormattingEnabled = true;
            this.gameListCombo.Items.AddRange(new object[] {
            "Please log in to join a game"});
            this.gameListCombo.Location = new System.Drawing.Point(19, 192);
            this.gameListCombo.Name = "gameListCombo";
            this.gameListCombo.Size = new System.Drawing.Size(172, 21);
            this.gameListCombo.TabIndex = 4;
            // 
            // lastNameTextbox
            // 
            this.lastNameTextbox.Location = new System.Drawing.Point(72, 38);
            this.lastNameTextbox.Name = "lastNameTextbox";
            this.lastNameTextbox.Size = new System.Drawing.Size(119, 20);
            this.lastNameTextbox.TabIndex = 6;
            this.lastNameTextbox.Text = "Tester";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Last Name";
            // 
            // aNumberTextbox
            // 
            this.aNumberTextbox.Location = new System.Drawing.Point(72, 64);
            this.aNumberTextbox.Name = "aNumberTextbox";
            this.aNumberTextbox.Size = new System.Drawing.Size(119, 20);
            this.aNumberTextbox.TabIndex = 8;
            this.aNumberTextbox.Text = "A00001234";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "A#";
            // 
            // aliasTextbox
            // 
            this.aliasTextbox.Location = new System.Drawing.Point(72, 90);
            this.aliasTextbox.Name = "aliasTextbox";
            this.aliasTextbox.Size = new System.Drawing.Size(119, 20);
            this.aliasTextbox.TabIndex = 10;
            this.aliasTextbox.Text = "test_fryarludwig";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Alias";
            // 
            // ipTextbox
            // 
            this.ipTextbox.Location = new System.Drawing.Point(72, 116);
            this.ipTextbox.Name = "ipTextbox";
            this.ipTextbox.Size = new System.Drawing.Size(68, 20);
            this.ipTextbox.TabIndex = 12;
            this.ipTextbox.Text = "52.3.213.61";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "IP / Port";
            // 
            // portTextbox
            // 
            this.portTextbox.Location = new System.Drawing.Point(146, 116);
            this.portTextbox.Name = "portTextbox";
            this.portTextbox.Size = new System.Drawing.Size(45, 20);
            this.portTextbox.TabIndex = 14;
            this.portTextbox.Text = "12000";
            // 
            // joinButton
            // 
            this.joinButton.Location = new System.Drawing.Point(72, 219);
            this.joinButton.Name = "joinButton";
            this.joinButton.Size = new System.Drawing.Size(75, 23);
            this.joinButton.TabIndex = 15;
            this.joinButton.Text = "Join Game";
            this.joinButton.UseVisualStyleBackColor = true;
            // 
            // mainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 270);
            this.Controls.Add(this.joinButton);
            this.Controls.Add(this.portTextbox);
            this.Controls.Add(this.ipTextbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.aliasTextbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.aNumberTextbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lastNameTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gameListCombo);
            this.Controls.Add(this.firstNameTextbox);
            this.Controls.Add(this.loginButton);
            this.Controls.Add(this.label1);
            this.Name = "mainWindow";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.TextBox firstNameTextbox;
        private System.Windows.Forms.ComboBox gameListCombo;
        private System.Windows.Forms.TextBox lastNameTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox aNumberTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox aliasTextbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ipTextbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox portTextbox;
        private System.Windows.Forms.Button joinButton;
    }
}

