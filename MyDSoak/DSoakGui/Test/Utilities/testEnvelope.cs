﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using DSoakGui.Utilities;
using Messages.RequestMessages;
using NUnit.Framework;

namespace DSoakGui.Test.Utilities
{
    [TestFixture]
    class testEnvelope
    {
        [Test]
        public void constructor()
        {
            Envelope testEnvelope1 = new Envelope();

            Assert.IsTrue(testEnvelope1.MessageRead);
            Assert.AreEqual(0, testEnvelope1.Address.Port);
            Assert.AreEqual("0.0.0.0", testEnvelope1.Address.Address.ToString());

            IPEndPoint testEndpoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 22);
            LoginRequest testRequest = new LoginRequest();

            Envelope testEnvelope2 = new Envelope(testEndpoint, testRequest);

            Assert.IsFalse(testEnvelope2.MessageRead);
            Assert.AreEqual(22, testEnvelope2.Address.Port);
            Assert.AreEqual("127.0.0.1", testEnvelope2.Address.Address.ToString());
            Assert.AreEqual(testRequest.GetType(), testEnvelope2.Message.GetType());
        }
    }
}
