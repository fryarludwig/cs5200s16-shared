﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using NUnit.Framework;
using DSoakGui.Utilities;

namespace DSoakGui.Test.Utilities
{
    [TestFixture]
    public class testConversation
    {
        [Test]
        public void constructor()
        {
            Conversation testConversation = new Conversation();

            Assert.AreEqual(0, testConversation.MaxRetries);
            Assert.AreEqual(0, testConversation.Timeout);
            Assert.IsFalse(testConversation.isActive());

            testConversation.start();
            Thread.Sleep(300);
            Assert.IsTrue(testConversation.isActive());

            testConversation.stop();
            Assert.IsFalse(testConversation.isActive());
        }
    }
}
