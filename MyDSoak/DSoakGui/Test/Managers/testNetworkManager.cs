﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using NUnit.Framework;

using DSoakGui.Managers;
using DSoakGui.Utilities;

namespace DSoakGui.Test.Managers
{
    [TestFixture]
    public class testNetworkManager
    {
        [Test]
        public void sendAndReceive()
        {
            IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5550);
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5555);

            NetworkManager testLocal = new NetworkManager();
            Assert.IsFalse(testLocal.isActive());

            UdpClient testRemote = new UdpClient(remoteEndPoint.Port);

            testLocal.LocalPort = localEndPoint.Port;
            testLocal.start();
            Assert.IsTrue(testLocal.isActive());

            string testString = "This is a test string";
            byte[] testBytes = Encoding.ASCII.GetBytes(testString);
            testRemote.Send(testBytes, testBytes.Length, localEndPoint);

            Thread.Sleep(300);

            // We sent a string - it should reject this received byte array
            Assert.IsTrue(testLocal.InboundQueue.IsEmpty);

            LoginRequest testMessage = new LoginRequest();
            testMessage.Identity = new SharedObjects.IdentityInfo();
            testMessage.Identity.FirstName = "Kenny";
            testMessage.Identity.LastName = "Fryar-Ludwig";
            testMessage.Identity.ANumber = "A012...";
            testMessage.Identity.Alias = "kennethj";
            testMessage.ProcessLabel = "NA";
            testMessage.ProcessType = new SharedObjects.ProcessInfo.ProcessType();
            testBytes = testMessage.Encode();
            testRemote.Send(testBytes, testBytes.Length, localEndPoint);

            Thread.Sleep(300);

            // We sent an actual message - this should be valid
            Envelope receivedEnvelope;
            Assert.IsFalse(testLocal.InboundQueue.IsEmpty);
            Assert.IsTrue(testLocal.InboundQueue.TryDequeue(out receivedEnvelope));

            Message receivedMessage = receivedEnvelope.Message;

            Assert.AreEqual(receivedMessage.GetType(), testMessage.GetType());

            LoginRequest receivedRequest = (LoginRequest)receivedMessage;
            Assert.AreEqual(receivedRequest.Identity.FirstName, testMessage.Identity.FirstName);
            Assert.AreEqual(receivedRequest.Identity.LastName, testMessage.Identity.LastName);
            Assert.AreEqual(receivedRequest.Identity.ANumber, testMessage.Identity.ANumber);
            Assert.AreEqual(receivedRequest.Identity.Alias, testMessage.Identity.Alias);
            Assert.AreEqual(receivedRequest.ProcessLabel, testMessage.ProcessLabel);

            LoginReply replyMessage = new LoginReply();
            replyMessage.Success = true;
            replyMessage.Note = "You send a message!";
            replyMessage.ProcessInfo = new SharedObjects.ProcessInfo();

            Envelope toSend = new Envelope(remoteEndPoint, replyMessage);
            testLocal.OutboundQueue.Enqueue(toSend);

            Thread.Sleep(300);

            IPEndPoint recvdEndpoint = new IPEndPoint(localEndPoint.Address, localEndPoint.Port);
            testBytes = testRemote.Receive(ref recvdEndpoint);
            receivedMessage = Message.Decode(testBytes);

            LoginReply receivedReply = (LoginReply)replyMessage;

            Assert.IsTrue(receivedReply.Success);
            Assert.AreEqual(receivedReply.Note, replyMessage.Note);

            testLocal.stop();
            testRemote.Close();
        }

        [Test]
        public void connectAndDisconnect()
        {
            NetworkManager testLocal = new NetworkManager();
            Assert.IsFalse(testLocal.isActive());

            testLocal.LocalPort = 5553;

            testLocal.start();
            Assert.IsTrue(testLocal.isActive());

            Assert.IsTrue(testLocal.OutboundQueue.IsEmpty);
            Assert.IsTrue(testLocal.InboundQueue.IsEmpty);

            System.Threading.Thread.Sleep(500);

            Assert.IsTrue(testLocal.OutboundQueue.IsEmpty);
            Assert.IsTrue(testLocal.InboundQueue.IsEmpty);

            testLocal.stop();
            Assert.IsFalse(testLocal.isActive());

            testLocal.start();
            Assert.IsTrue(testLocal.isActive());

            Assert.IsTrue(testLocal.OutboundQueue.IsEmpty);
            Assert.IsTrue(testLocal.InboundQueue.IsEmpty);

            System.Threading.Thread.Sleep(500);

            Assert.IsTrue(testLocal.OutboundQueue.IsEmpty);
            Assert.IsTrue(testLocal.InboundQueue.IsEmpty);

            testLocal.stop();
            Assert.IsFalse(testLocal.isActive());
        }
    }
}
