﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using NUnit.Framework;

using DSoakGui.Common;
using DSoakGui.Utilities;

namespace DSoakGui.Test.Common
{
    class ThreadedTestClass : Threaded
    {
        public ThreadedTestClass()
        {
            RunCounter = 0;
            DerivedStopCounter = 50;
        }

        protected override void run()
        {
            while (!threadDone)
            {
                RunCounter++;
                Thread.Sleep(10);
            }
        }

        protected override void derivedStop()
        {
            int tempVar = RunCounter;

            while (tempVar > 0)
            {
                tempVar--;
                DerivedStopCounter++;
            }
        }

        public int RunCounter;
        public int DerivedStopCounter;
    }

    [TestFixture]
    class testThreaded
    {
        [Test]
        public void startAndStop()
        {
            Threaded testThread = new Threaded();

            Assert.IsFalse(testThread.isActive());

            testThread.start();
            Thread.Sleep(200);
            Assert.IsFalse(testThread.isActive());

            testThread.stop();
            Assert.IsFalse(testThread.isActive());
        }

        [Test]
        public void runAndDerivedStop()
        {
            ThreadedTestClass testThread = new ThreadedTestClass();

            Assert.IsFalse(testThread.isActive());

            testThread.start();
            Thread.Sleep(200);

            testThread.stop();
            Assert.IsFalse(testThread.isActive());

            Assert.IsTrue(testThread.RunCounter > 10);
            Assert.IsTrue(testThread.DerivedStopCounter > 10);
        }
    }
}
