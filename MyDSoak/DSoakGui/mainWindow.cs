﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using log4net;
using Messages.RequestMessages;
//using MyDSoak.Sockets;

namespace DSoakGui
{
    public partial class mainWindow : Form
    {
        public mainWindow()
        {
            InitializeComponent();
            m_logger = LogManager.GetLogger(typeof(mainWindow));
            m_logger.Info("Main window initialized");
        }

        private async void onLoginButtonClicked(object sender, EventArgs e)
        {
            //int portNumber = -1;

            //if (Int32.TryParse(portTextbox.Text, out portNumber))
            //{
            //    if (m_serverConnection.setIpAddress(ipTextbox.Text))
            //    {
            //        m_serverConnection.LocalPort = portNumber;
            //        await Task.Factory.StartNew(() => m_serverConnection.toggleConnection());
            //    }
            //}

            //if (m_serverConnection.isRunning())
            //{
            //    LoginRequest loginMessage = new LoginRequest();
            //    loginMessage.Identity.FirstName = firstNameTextbox.Text;
            //    loginMessage.Identity.LastName = lastNameTextbox.Text;
            //    loginMessage.Identity.ANumber = aNumberTextbox.Text;
            //    loginMessage.Identity.Alias = aliasTextbox.Text;

            //    m_serverConnection.OutboundQueue.Enqueue(loginMessage);
            //}
        }

        public void updateGameList()
        {
        }

        public void updateConnectionStatus(string message)
        {
            gameListCombo.Items.Add(message);
            gameListCombo.Update();
        }

        //private udpSocket m_serverConnection;
        private ILog m_logger;
    }
}
