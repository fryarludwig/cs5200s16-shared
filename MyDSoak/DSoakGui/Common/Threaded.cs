﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DSoakGui.Utilities;

namespace DSoakGui.Common
{
    class Threaded
    {
        public Threaded()
        {
            threadDone = true;
        }

        public void start()
        {
            if (threadDone)
            {
                threadDone = false;
                runThread = new Thread(run);
                runThread.Start();
            }
            else
            {
                Logger.log.Info("Threaded: Cannot intiate thread, it is already active");
            }
        }

        public void stop()
        {
            if (!threadDone)
            {
                Logger.log.Info("Threaded: Closing Thread");
                derivedStop();
                threadDone = true;
                runThread.Join();
            }
            else
            {
                Logger.log.Warn("Threaded: Current object is not active, cannot close");
            }
        }

        protected virtual void run()
        {
            Logger.log.Error("Threaded: run function was not overloaded.");
            stop();
        }

        protected virtual void derivedStop()
        {
            Logger.log.Info("Threaded: Derived stop not implemented.");
        }

        public bool isActive()
        {
            return !threadDone;
        }

        protected Thread runThread;
        protected bool threadDone;
    }
}
