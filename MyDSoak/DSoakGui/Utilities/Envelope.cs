﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

using Messages;

namespace DSoakGui.Utilities
{
    class Envelope
    {
        public Envelope()
        {
            Address = new IPEndPoint(IPAddress.Parse("0.0.0.0"), 0);
            Message = new Message();
            MessageRead = true;
        }

        public Envelope(IPEndPoint tempAddress, Message tempMessage)
        {
            Address = tempAddress;
            Message = tempMessage;
            MessageRead = false;
        }

        public bool MessageRead;
        public IPEndPoint Address;
        public Message Message;
    }
}
