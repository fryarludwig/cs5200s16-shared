using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using DSoakGui.Common;

namespace DSoakGui.Utilities
{
    class Conversation : Threaded
    {
        internal Conversation()
        {
            Id = 0;
            Timeout = 0;
            MaxRetries = 0;
            MessageQueue = new ConcurrentQueue<Envelope>();
            ArchivedMessageQueue = new ConcurrentQueue<Envelope>();
        }

        protected override void run()
        {
            Logger.log.Info("Conversation: Starting a conversation run thread");
            threadDone = false;

            while (!threadDone)
            {
                if (MessageQueue.IsEmpty)
                {
                    Thread.Sleep(500);
                }
                else
                {
                    Envelope currentMessage;
                    MessageQueue.TryDequeue(out currentMessage);

                    // if valid
                    // do something
                    // send reply if needed
                }
            }

            Logger.log.Info("Conversation: Ending a conversation");
        }

        public UInt32 Id;
        public UInt32 Timeout;
        public UInt32 MaxRetries;

        public ConcurrentQueue<Envelope> MessageQueue;
        protected ConcurrentQueue<Envelope> ArchivedMessageQueue;
    }
}
