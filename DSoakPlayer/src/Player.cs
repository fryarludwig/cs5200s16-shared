﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using SharedObjects;

namespace DSoakPlayer
{
    public class Player : Threaded
    {
        #region Constructor/Destructor
        public Player() : base("Player")
        {
            Properties = SharedProperties.Instance;
            AvailableGameList = new List<GameInfo>();
            ConversationHandler = new ConversationManager();
            ConversationHandler.Start();

            Logger.Trace("Initialized Player");
        }
        protected override void DerivedStop()
        {
            Logger.Trace("Calling Derived Stop");
            ConversationHandler.Stop();
        }

        #endregion

        #region GUI interface functions
        public bool GuiLoginHelper()
        {
            base.Start();

            // Wait for 5 seconds, or for a value of true
            int checkCounter = 10;
            while (Process.Status != ProcessInfo.StatusCode.Registered && checkCounter-- > 0)
            {
                Logger.Trace("Attempting to log in");
                Thread.Sleep(500);
            }

            return Process.Status == ProcessInfo.StatusCode.Registered;
        }

        public bool GuiLogoutHelper()
        {
            if (base.ContinueThread)
            {
                base.Stop();
            }

            // Wait for 5 seconds, or for a value of true
            int checkCounter = 5;
            while (Process != null && checkCounter-- > 0)
            {
                Logger.Trace("Attempting to log player out");
                Thread.Sleep(1000);
            }

            return Process == null || Process.Status == ProcessInfo.StatusCode.Terminating;
        }

        public List<GameInfo> GuiFetchGameList()
        {
            return Properties.GameInfoList;
        }
        #endregion


        protected override void Run()
        {

            while (ContinueThread)
            {
                if (Process.Status == ProcessInfo.StatusCode.Unknown || Process.Status == ProcessInfo.StatusCode.NotInitialized)
                {
                    Login();
                }
                else if (Process.Status == ProcessInfo.StatusCode.Registered)
                {
                    if (AvailableGameList.Count > 0)
                    {
                        JoinGame();
                    }
                    else
                    {
                        UpdateGameList();
                    }
                }
                else if (Process.Status == ProcessInfo.StatusCode.JoinedGame)
                {
                    PlayGame();
                }
                else if (Process.Status == ProcessInfo.StatusCode.Terminating)
                {
                    break;
                }

                Thread.Sleep(250);
            }

            Logout(2000);
        }

        protected void Login()
        {
            if (Process.Status != ProcessInfo.StatusCode.Initializing)
            {
                Logger.Info("Requesting a login");
                Process.Status = ProcessInfo.StatusCode.Initializing;
                ConversationHandler.Execute(ConversationFactory.CreateType<LoginConversation>());
            }
            else
            {
                Logger.Warn("Status is '" + Process.StatusString + "', waiting for login to complete");
            }
        }

        protected void Logout(int Timeout)
        {
            Logger.Info("Requesting logout");
            Process.Status = ProcessInfo.StatusCode.Terminating;
            ConversationHandler.Execute(ConversationFactory.CreateType<LogoutConversation>());

            int timeSegement = Timeout / 5;
            while (Timeout > 0)
            {
                Thread.Sleep(timeSegement);
                Timeout -= timeSegement;
            }

        }

        protected void UpdateGameList()
        {
            Logger.Info("Requesting game list updates");
            Conversation gameConversation = ConversationFactory.CreateType<GameListConversation>();
            ConversationHandler.Execute(gameConversation);
            while (gameConversation.IsActive())
            {
                Thread.Sleep(2);
            }
        }

        protected void JoinGame()
        {
            // if there is at least on available game
            // Pick a game from the avaiable game list
            // try to complete a join game conversation the game manager of the selected game

            // if successfully, update status and save game joined in currentGameInfo
        }

        protected void PlayGame()
        {

        }

        public void SetRegistryEndpoint(string ip, int port)
        {
            RegistryEndpoint = new IPEndPoint(IPAddress.Parse(ip), port);
        }

        #region Public Member Variables

        public IPEndPoint RegistryEndpoint
        {
            get
            {
                return Properties.RegistryEndpoint;
            }
            set
            {
                Properties.RegistryEndpoint = value;
            }
        }

        public IdentityInfo Identity
        {
            get
            {
                return Properties.Identity;
            }
        }
        public ProcessInfo Process
        {
            get
            {
                return Properties.Process;
            }
        }
        public SharedProperties Properties { get; }
        public List<GameInfo> AvailableGameList { get; }
        public GameInfo ActiveGame { get; set; }
        #endregion

        #region Protected and Private Member Variables
        protected int LoginRetries;
        protected ConversationManager ConversationHandler;
        #endregion
    }
}
