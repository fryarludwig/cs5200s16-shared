﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using SharedObjects;
using Utils;

namespace DSoakPlayer
{
    class GameListConversation : Conversation
    {
        public GameListConversation() : base("GameListConv")
        {
        }


        protected override void Run()
        {
            int availableRetries = 5;
            Envelope tempEnvelope;

            while (ContinueThread)
            {
                if (!NewMessages.IsEmpty && NewMessages.TryDequeue(out tempEnvelope))
                {
                    if (tempEnvelope.Message.GetType() == typeof(GameListReply))
                    {
                        GameListReply reply = (GameListReply)tempEnvelope.Message;
                        string success = (reply.Success) ? "Successful" : "Failed";
                        Logger.Info(success + "GameList response with note: " + reply.Note);
                        if (reply.GameInfo.Length > 0)
                        {
                            Properties.GameInfoList = new List<GameInfo>(reply.GameInfo);
                        }
                    }
                    else
                    {
                        Logger.Info("Received unexpected message: " + tempEnvelope.Message.ToString());
                    }
                }
                else if (availableRetries-- > 0)
                {
                    Logger.Info("Sending game list request");
                    Envelope newEnvelope = new Envelope(Properties.RegistryEndpoint, CreateMessage());

                    Communicator.Send(PopulateEnvelope());
                    Thread.Sleep(Timeout);
                }
                else
                {
                    Logger.Warn("Failed to get game list reply");
                    Stop();
                }

            }

            Logger.Info("Conversation: Ending a conversation");
        }

        protected override Message CreateMessage()
        {
            GameListRequest message = new GameListRequest();
            message.SetMessageAndConversationNumbers(Id);
            message.ConvId.Pid = Process.ProcessId;
            message.MsgId.Pid = Process.ProcessId;

            return message;
        }
    }
}
