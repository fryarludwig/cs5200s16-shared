﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using SharedObjects;
using Utils;

namespace DSoakPlayer
{
    public class BuyBalloonConversation : Conversation
    {
        public BuyBalloonConversation() : base("BuyBalloon")
        {
        }


        protected override void Run()
        {

        }

        protected override Message CreateMessage()
        {
            BuyBalloonRequest message = new BuyBalloonRequest();
            message.ConvId = Id;
            message.MsgId = MessageNumber.Create();
            message.Penny = new Penny(); // TODO Fix this, add other things

            return message;
        }
    }
}
