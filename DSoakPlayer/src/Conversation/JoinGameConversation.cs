﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;
using SharedObjects;
using Utils;

namespace DSoakPlayer
{
    class JoinGameConversation : Conversation
    {
        public JoinGameConversation() : base("JoinGame")
        {
        }


        protected override void Run()
        {
            Envelope tempEnvelope;

            while (ContinueThread)
            {
                if (!NewMessages.IsEmpty)
                {
                    Logger.Info("Received some kind of response");
                    if (NewMessages.TryDequeue(out tempEnvelope))
                    {
                        if (tempEnvelope.Message.GetType() == typeof(JoinGameReply))
                        {
                            JoinGameReply message = (JoinGameReply)tempEnvelope.Message;
                            Logger.Info("Received Login response: " + message.ConvId.Pid.ToString());
                            Communicator.Send(PopulateEnvelope());
                        }
                        else
                        {
                            Logger.Info("Received unexpected message: " + tempEnvelope.Message.ToString());
                        }
                    }
                }

                Thread.Sleep(500);
            }

            Logger.Info("Conversation: Ending a conversation");
        }

        protected override Message CreateMessage()
        {
            JoinGameRequest message = new JoinGameRequest();
            message.ConvId = Id;
            message.MsgId = MessageNumber.Create();
            message.Process = Process.Clone();

            return message;
        }
    }
}
