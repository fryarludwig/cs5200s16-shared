﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using SharedObjects;
using Messages;
using Messages.ReplyMessages;
using Messages.RequestMessages;

namespace DSoakPlayer
{
    public class ConversationManager : Threaded
    {
        public ConversationManager() : base("ConversationManager")
        {
            ConversationDictionary = new Dictionary<MessageNumber, Conversation>();
            Communicator = CommunicationManager.Instance;
        }
        protected override void DerivedStop()
        {
            lock (ConvQueueLock)
            {
                foreach (Conversation conversation in ConversationDictionary.Values)
                {
                    conversation.Stop();
                }
            }

            Communicator.Stop();
        }

        public void Execute(Conversation conv)
        {
            lock (ConvQueueLock)
            {
                conv.Start();
                ConversationDictionary[conv.Id] = conv;
            }
        }

        protected override void Run()
        {
            Envelope envelope;

            while (ContinueThread)
            {
                if (Communicator.ReplyWaiting)
                {
                    Logger.Info("Reply message waiting - attempting to retrieve");
                    if (Communicator.Receive(out envelope) && envelope != null)
                    {
                        if (ConversationDictionary.ContainsKey(envelope.ConvId))
                        {
                            ConversationDictionary[envelope.ConvId].NewMessages.Enqueue(envelope);
                        }
                        else if (InitiateConversation(envelope))
                        {
                            ConversationDictionary[envelope.ConvId].NewMessages.Enqueue(envelope);
                        }
                        else
                        {
                            Logger.Error("Received a message of unknown type. Cannot act on conversation " + envelope.ConvId.ToString());
                        }
                    }
                }


                Thread.Sleep(250);
            }

            Logger.Info("Conversation manager has closed");
        }

        protected bool InitiateConversation(Envelope envelope)
        {
            bool success = false;

            if (envelope.Message.GetType() == typeof(AliveRequest))
            {
                Conversation aliveConv = ConversationFactory.CreateType<AliveConversation>();
                aliveConv.Id = envelope.Message.ConvId;
                Execute(aliveConv);
                success = true;
            }

            return success;
        }


        private Dictionary<MessageNumber, Conversation> ConversationDictionary;
        private CommunicationManager Communicator;
        private object ConvQueueLock = new object();
    }
}
