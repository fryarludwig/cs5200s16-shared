﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Messages;
using SharedObjects;

namespace DSoakPlayer
{
    public static class ConversationFactory
    {
        static ConversationFactory()
        {
            Retries = 5;
            Timeout = 1000;
            Properties = SharedProperties.Instance;
        }

        public static Conversation CreateType<T>() where T : Conversation, new()
        {
            Conversation newConversation = new T();
            newConversation.MaxRetries = Retries;
            newConversation.Timeout = Timeout;

            return newConversation;
        }

        #region Public member variables
        public static UInt32 Retries { get; set; }
        public static int Timeout { get; set; }
        public static SharedProperties Properties { get; set; }
        #endregion
    }
}
