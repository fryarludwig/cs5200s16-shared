﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using SharedObjects;

namespace DSoakPlayer
{
    public partial class DSoakPlayerWindow : Form
    {
        public DSoakPlayerWindow()
        {
            InitializeComponent();

            ActivePlayer = new Player();
            gameListCombo.SelectedIndex = 0;

            ApplyLogFilters();

            Logger.ConsoleOutput = true;
            Logger.GuiOutput = true;
            Logger.FileOutput = true;
            Task.Factory.StartNew(RunLoop);
        }

        public void KillChildren()
        {
            ActivePlayer.Stop();
        }

        protected void RunLoop()
        {
            int NumberOfItems = LogTextArea.ClientSize.Height / LogTextArea.ItemHeight;
            bool keepGoing = true;
            LogItem message;

            while (keepGoing)
            {
                if (!GuiLogQueue.IsEmpty && GuiLogQueue.TryDequeue(out message))
                {
                    Invoke((MethodInvoker)delegate
                    {
                        try
                        {
                            if (LogPrintDictionary[message.LogLevel])
                            {
                                LogTextArea.Items.Add(message.LogMessage);
                                if (LogTextArea.TopIndex == LogTextArea.Items.Count - NumberOfItems - 1)
                                {
                                    //The item at the top when you can just see the bottom item
                                    LogTextArea.TopIndex = LogTextArea.Items.Count - NumberOfItems + 1;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Logger.GuiOutput = false;
                            Logger.ConsoleOutput = true;
                            Logger.Error(e.Message);
                            keepGoing = false;
                        }
                    });
                }

                Thread.Sleep(100);
            }
        }


        private void ApplyLogFilters()
        {
            foreach (ToolStripMenuItem item in contextMenuStrip1.Items)
            {
                LogPrintDictionary[LogLevelMapper.LevelFromString(item.Text)] = item.Checked;
            }
        }

        private bool ValidateLoginInformation()
        {
            bool validInformation = true;
            InputErrorProvider.Clear();

            IEnumerable<TextBox> textBoxes = Controls.OfType<TextBox>();
            foreach (TextBox box in textBoxes)
            {
                if (string.IsNullOrWhiteSpace(box.Text))
                {
                    InputErrorProvider.SetError(box, "Input cannot be blank");
                    validInformation = false;
                }
            }

            int throwaway = 0;
            if (!int.TryParse(portTextbox.Text, out throwaway))
            {
                validInformation = false;
                InputErrorProvider.SetError(portTextbox, "Please enter a valid port number");
            }

            return validInformation;
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            loginButton.Enabled = false;
            if (loginButton.Text == "Login" && ValidateLoginInformation())
            {
                Logger.Trace("Login information passed basic validation");
                InitializePlayerInformation();
                PerformLogin().ContinueWith((t) => UpdateLoginStatus(t.Result), TaskScheduler.FromCurrentSynchronizationContext());
            }
            else if (loginButton.Text == "Logout")
            {
                PerformLogout().ContinueWith((t) => UpdateLoginStatus(t.Result), TaskScheduler.FromCurrentSynchronizationContext());
            }
            else
            {
                Logger.Info("User entered invalid information");
                loginButton.Enabled = true;
            }
        }

        private void InitializePlayerInformation()
        {
            ActivePlayer.Identity.FirstName = firstNameTextbox.Text;
            ActivePlayer.Identity.LastName = lastNameTextbox.Text;
            ActivePlayer.Identity.ANumber = aNumberTextbox.Text;
            ActivePlayer.Identity.Alias = aliasTextbox.Text;

            ActivePlayer.SetRegistryEndpoint(ipTextbox.Text, int.Parse(portTextbox.Text));
        }

        private Task<bool> PerformLogin()
        {
            Logger.Trace("Calling Login function for player");

            return Task.Factory.StartNew<bool>(ActivePlayer.GuiLoginHelper);
        }

        private Task<bool> PerformLogout()
        {
            Logger.Trace("Requesting user log out");

            return Task.Factory.StartNew<bool>(ActivePlayer.GuiLogoutHelper);
        }

        private void UpdateLoginStatus(bool loggedIn)
        {
            if (loggedIn)
            {
                Logger.Trace("Player is logged in");
                loginButton.Text = "Logout";
            }
            else
            {
                Logger.Trace("Player is logged out");
                loginButton.Text = "Login";

                gameListCombo.Items.Clear();
                gameListCombo.Items.Add("Login to select a game");
            }

            loginButton.Enabled = true;
        }

        private void refreshGamesButton_Click(object sender, EventArgs e)
        {
            Logger.Info("Refresh Game List button clicked");

            gameListCombo.Items.Clear();
            List<GameInfo> GameInfoList = ActivePlayer.GuiFetchGameList();
            for (int i = 0; i < GameInfoList.Count; i++)
            {
                gameListCombo.Items.Add(GameInfoList[i].GameId);
            }

            gameListCombo.SelectedIndex = 0;
        }

        public void UpdateGuiStatus()
        {
            // Call a method to update the gui
        }

        protected ErrorProvider InputErrorProvider = new ErrorProvider();
        protected LogUtility Logger = new LogUtility("DSoak GUI");
        protected Player ActivePlayer { get; }
        public static ConcurrentQueue<LogItem> GuiLogQueue = new ConcurrentQueue<LogItem>();
        public static ConcurrentDictionary<Level, bool> LogPrintDictionary = new ConcurrentDictionary<Level, bool>();

        private void checkBox1_CheckedChanged(object sender, EventArgs e) { }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e) { }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripDropDownClosingEventArgs e) { }

        private void contextMenuStrip1_ItemClicked(object sender, EventArgs e)
        {
            ApplyLogFilters();
        }
    }
}
