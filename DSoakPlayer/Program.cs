﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DSoakPlayer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            DSoakPlayerWindow GuiWindow = new DSoakPlayerWindow();
            Application.Run(GuiWindow);
            GuiWindow.KillChildren();

            Application.Exit();
        }
    }
}
