﻿namespace DSoakPlayer
{
    partial class DSoakPlayerWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DSoakPlayerWindow));
            this.portTextbox = new System.Windows.Forms.TextBox();
            this.ipTextbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.aliasTextbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.aNumberTextbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lastNameTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gameListCombo = new System.Windows.Forms.ComboBox();
            this.firstNameTextbox = new System.Windows.Forms.TextBox();
            this.loginButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LogTextArea = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.errorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.warnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.traceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.playerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // portTextbox
            // 
            this.portTextbox.Location = new System.Drawing.Point(83, 184);
            this.portTextbox.Name = "portTextbox";
            this.portTextbox.Size = new System.Drawing.Size(45, 20);
            this.portTextbox.TabIndex = 28;
            this.portTextbox.Text = "12000";
            this.portTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ipTextbox
            // 
            this.ipTextbox.Location = new System.Drawing.Point(9, 184);
            this.ipTextbox.Name = "ipTextbox";
            this.ipTextbox.Size = new System.Drawing.Size(68, 20);
            this.ipTextbox.TabIndex = 27;
            this.ipTextbox.Text = "52.3.213.61";
            this.ipTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "IP / Port";
            // 
            // aliasTextbox
            // 
            this.aliasTextbox.Location = new System.Drawing.Point(9, 145);
            this.aliasTextbox.Name = "aliasTextbox";
            this.aliasTextbox.Size = new System.Drawing.Size(119, 20);
            this.aliasTextbox.TabIndex = 25;
            this.aliasTextbox.Text = "RobZombie";
            this.aliasTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Alias";
            // 
            // aNumberTextbox
            // 
            this.aNumberTextbox.Location = new System.Drawing.Point(9, 106);
            this.aNumberTextbox.Name = "aNumberTextbox";
            this.aNumberTextbox.Size = new System.Drawing.Size(119, 20);
            this.aNumberTextbox.TabIndex = 23;
            this.aNumberTextbox.Text = "A01284981";
            this.aNumberTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "A#";
            // 
            // lastNameTextbox
            // 
            this.lastNameTextbox.Location = new System.Drawing.Point(9, 67);
            this.lastNameTextbox.Name = "lastNameTextbox";
            this.lastNameTextbox.Size = new System.Drawing.Size(119, 20);
            this.lastNameTextbox.TabIndex = 21;
            this.lastNameTextbox.Text = "Fryar-Ludwig";
            this.lastNameTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Last Name";
            // 
            // gameListCombo
            // 
            this.gameListCombo.FormattingEnabled = true;
            this.gameListCombo.Items.AddRange(new object[] {
            "Log in to join game"});
            this.gameListCombo.Location = new System.Drawing.Point(9, 247);
            this.gameListCombo.Name = "gameListCombo";
            this.gameListCombo.Size = new System.Drawing.Size(119, 21);
            this.gameListCombo.TabIndex = 19;
            // 
            // firstNameTextbox
            // 
            this.firstNameTextbox.Location = new System.Drawing.Point(9, 28);
            this.firstNameTextbox.Name = "firstNameTextbox";
            this.firstNameTextbox.Size = new System.Drawing.Size(119, 20);
            this.firstNameTextbox.TabIndex = 18;
            this.firstNameTextbox.Tag = "";
            this.firstNameTextbox.Text = "Kenneth";
            this.firstNameTextbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // loginButton
            // 
            this.loginButton.Location = new System.Drawing.Point(9, 210);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(119, 23);
            this.loginButton.TabIndex = 17;
            this.loginButton.Text = "Login";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(44, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "First Name";
            // 
            // LogTextArea
            // 
            this.LogTextArea.CausesValidation = false;
            this.LogTextArea.ContextMenuStrip = this.contextMenuStrip1;
            this.LogTextArea.Font = new System.Drawing.Font("Monospac821 BT", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LogTextArea.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LogTextArea.FormattingEnabled = true;
            this.LogTextArea.ItemHeight = 14;
            this.LogTextArea.Location = new System.Drawing.Point(134, 12);
            this.LogTextArea.Name = "LogTextArea";
            this.LogTextArea.ScrollAlwaysVisible = true;
            this.LogTextArea.Size = new System.Drawing.Size(623, 270);
            this.LogTextArea.TabIndex = 32;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.errorToolStripMenuItem,
            this.warnToolStripMenuItem,
            this.infoToolStripMenuItem,
            this.traceToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(103, 92);
            this.contextMenuStrip1.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this.contextMenuStrip1_ItemClicked);
            this.contextMenuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStrip1_ItemClicked);
            // 
            // errorToolStripMenuItem
            // 
            this.errorToolStripMenuItem.Checked = true;
            this.errorToolStripMenuItem.CheckOnClick = true;
            this.errorToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.errorToolStripMenuItem.Name = "errorToolStripMenuItem";
            this.errorToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.errorToolStripMenuItem.Text = "Error";
            this.errorToolStripMenuItem.CheckedChanged += new System.EventHandler(this.contextMenuStrip1_ItemClicked);
            // 
            // warnToolStripMenuItem
            // 
            this.warnToolStripMenuItem.Checked = true;
            this.warnToolStripMenuItem.CheckOnClick = true;
            this.warnToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.warnToolStripMenuItem.Name = "warnToolStripMenuItem";
            this.warnToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.warnToolStripMenuItem.Text = "Warn";
            this.warnToolStripMenuItem.CheckedChanged += new System.EventHandler(this.contextMenuStrip1_ItemClicked);
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Checked = true;
            this.infoToolStripMenuItem.CheckOnClick = true;
            this.infoToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.infoToolStripMenuItem.Text = "Info";
            this.infoToolStripMenuItem.CheckedChanged += new System.EventHandler(this.contextMenuStrip1_ItemClicked);
            // 
            // traceToolStripMenuItem
            // 
            this.traceToolStripMenuItem.Checked = true;
            this.traceToolStripMenuItem.CheckOnClick = true;
            this.traceToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.traceToolStripMenuItem.Name = "traceToolStripMenuItem";
            this.traceToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.traceToolStripMenuItem.Text = "Trace";
            this.traceToolStripMenuItem.CheckedChanged += new System.EventHandler(this.contextMenuStrip1_ItemClicked);
            // 
            // playerBindingSource
            // 
            this.playerBindingSource.DataSource = typeof(DSoakPlayer.Player);
            // 
            // DSoakPlayerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(769, 293);
            this.Controls.Add(this.LogTextArea);
            this.Controls.Add(this.portTextbox);
            this.Controls.Add(this.ipTextbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.aliasTextbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.aNumberTextbox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lastNameTextbox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gameListCombo);
            this.Controls.Add(this.firstNameTextbox);
            this.Controls.Add(this.loginButton);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DSoakPlayerWindow";
            this.Text = "DSoakPlayer";
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox portTextbox;
        private System.Windows.Forms.TextBox ipTextbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox aliasTextbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox aNumberTextbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox lastNameTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox gameListCombo;
        private System.Windows.Forms.TextBox firstNameTextbox;
        private System.Windows.Forms.Button loginButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource playerBindingSource;
        protected System.Windows.Forms.ListBox LogTextArea;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem errorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem warnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem traceToolStripMenuItem;
    }
}

