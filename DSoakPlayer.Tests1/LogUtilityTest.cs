// <copyright file="LogUtilityTest.cs">Copyright ©  2016</copyright>
using System;
using DSoakPlayer;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DSoakPlayer.Tests
{
    /// <summary>This class contains parameterized unit tests for LogUtility</summary>
    [PexClass(typeof(LogUtility))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class LogUtilityTest
    {
        /// <summary>Test stub for Trace(String)</summary>
        [PexMethod]
        internal void TraceTest([PexAssumeUnderTest]LogUtility target, string logMessage)
        {
            target.Trace(logMessage);
            // TODO: add assertions to method LogUtilityTest.TraceTest(LogUtility, String)
        }
    }
}
